/**
 * 
 */

/**
 * @author NEELU
 *
 */
public class InheritanceDemoStarter {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
//		Shape s=new Shape();
//		s.rotate();
//		s.playSound();
//		s.setLength(12);
//		System.out.println("Shape length: "+s.getLength());
//		s.setHeight(10);
//		System.out.println("Shape height: "+s.getHeight());

		Shape s=new Square();			//polymorphism
		s.area();
		s.setHeight(555);
		System.out.println(s.getHeight());
		
		
		
		
		Square square=new Square();
		square.rotate();
		square.playSound();
		square.setLength(15);
		System.out.println("Square length: "+square.getLength());
		square.setHeight(110);
		System.out.println("Square height: "+square.getHeight());
		System.out.println("getHeightSquare: "+square.getHeightSquare());
		System.out.println("getHeightShape: "+square.getHeightShape());
		

		
		Amoeba amoeba=new Amoeba();
		amoeba.rotate();
		amoeba.playSound();
		
	}

}
