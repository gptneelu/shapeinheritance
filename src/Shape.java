/**
 * 
 */

/**
 * @author NEELU
 *
 */
public abstract class Shape
{
	abstract double area();
	private int length;
	int getLength()
	{
		return length;
	}
	void setLength(int l)
	{
		length=l;
	}
	
	int height;
	void setHeight(int h)
	{
		height=h;
	}
	int getHeight()
	{
		return height;
	}
	
	int getHeightShape()
	{
		return height;
	}
	void rotate()
	{
		System.out.println("Rotate shape");
	}
	void playSound()
	{
		System.out.println("Play mp3 file");
	}
}
